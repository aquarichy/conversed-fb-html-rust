# ConverseD

**ConverseD** is a Chat Log database, viewer and parser collection, written in Rust.
For a web-based version, see [ConverseD Web](https://gitlab.com/aquarichy/conversed-web).

Features:

- Database: uses SQLite or MariaDB/MySQL to store chat logs from various services
  in one location.
- Parsers: a collection of parsers for different services/client log formats.
  Also available as a library or as a WASM, e.g. for use by the Node.js-based `ConverseD Web`.
- CLI: a command-line utility, `conversed`, to parse, load into, and view chats from, a
  `ConverseD` database
- GUI: a GTK4-based viewer for chats and messages in a `ConverseD` database

## Available Parsers

- `meta_json`: Meta JSON for Facebook and Instagram JSON logs (recommended)
- `meta_html`: Meta HTML for Facebook and Instagram HTML logs (discouraged)
- `cicq`: centerICQ logs
- `signal_sqlite`: Signal logs from decrypted Signal backup SQLite DB (recommended)
- `signal_json`: Signal logs from custom JSON extracts (see big caveat below) (discouraged)
- `matrix_json`: Matrix JSON logs as exported from Element
- `sms`: SMS and MMS messages from Android, as exported by SMSBackupRestore

JSON or HTML logs from Meta services like **Facebook** or **Instagram** can be obtained
by logging into Facebook or Instagram, going to Settings, to your Accounts
center, to Download Your Information, and downloading Messages as JSON or HTML.
I strongly recommend you export them as JSON, as the structure is more explicit,
less likely to incur breaking changes (e.g. CSS class names), and provides
consistent, unambiguous per-message timestamps agnostic to timezone changes.

**centerICQ** was a multi-protocol open source chat client that could run on the
command-line and supported various protocols like MSN, ICQ, Yahoo! and more.  Note that it
did not encode the first person name in chat logs, so your messages will appear as "Me".

Usable **Signal** logs can be obtained by using Signal to export its encrypted backup file,
use a third-party tool to decrypt it, and locating the `database.sqlite` DB file in its
root directory.  That can be parsed using directly.

An indirect option exists using our `scripts/signal_export_to_json.sh` script
to extract a JSON subset of its data, and then run this project's parser on that JSON file.
Yikes.  See the *Signal caveat* section below for why.

## Available DB Backends

There is now support for two different backend databases:

- MySQL/MariaDB: the original backend, whose configuration is specified below in: `Database Configuration`, and requires a MySQL/MariaDB daemon running
- SQLite: new, stored in a local SQLite DB file, at `~/.local/share/conversed/conversed.sqlite`

The default is MySQL/MariaDB.  SQLite can be selected at runtime by passing `-s` or `--sqlite`.
The configuration file format will eventually change to allow run-time selection.

## Dependencies

Here is an explanation of our dependencies.

- Rust crates:
    - `roxmltree`: parse HTML files for Facebook
    - `wasm-bindgen`: generate bindings for WASM from Rust APIs, for WASM
    - serde:
        - `serde`: serialize Rust objects into JavaScript Objects and vice-versa, for WASM; also, deserialize TOML config file
        - `serd-wasm-bindgen`
        - `serde_json`: deserialize JSON strings, for parsing Meta JSON files
    - `chrono`: for representing date-time for messages
    - `clap`: for command-line argument parsing
    - `sqlx`: for experimental DB access
    - `async-std`: for async support for sqlx
    - `toml`: for parsing TOML config file
    - `quick-xml`: for parsing SMS messages
    - `gtk4`: used to provide a simple chat log viewer
    - `console_error_panic_hook`: allows panic calls to share their message to
       WASM users through a browser or Node.js's `console.error` API
- `wasm-pack`: Rust-to-WASM binary for compiling WASM from Rust

## Installation

### Flatpak

``` shell
make flatpak_test
```

This builds a Flatpak for Conversed and does an install.  Eventually, one will
be published to Flathub.  In this version, Conversed will use an SQLite database
stored at `~/.var/app/org.kosmokaryote.conversed/data/conversed.sqlite`.

### to ~/.local

``` shell
make install
```

This installs files under ~/.local and should provide a 'ConverseD' launcher with icon for your desktop.
Installed files should include at least:

- `~/.local/bin/conversed`
- `~/.local/share/applications/org.kosmokaryote.conversed.desktop`
- `~/.local/share/icons/hicolor/scalable/apps/org.kosmokaryote.conversed.svg`

Run the following to deinstall those files:

``` shell
make uninstall
```

By default, the .desktop file uses the `-s` option to run Conversed with an SQLite database
found at `~/.local/share/conversed/conversed.sqlite`.  Just running the binary from the CLI
currently assumes there will be a MySQL/MariaDB though.  (This default will likely change
in the future to default to SQLite instead.)

### to ~/.cargo (just the binary)

``` shell
cargo install --path .
```

This will place the `conversed` binary into `~/.cargo/bin`, which you should
add to your PATH variable if you'd like to run this program by simply calling
the binary name.  No icon or .desktop file will be installed.

See *MySQL/MariaDB Database Configuration* section below to make use of DB
functionality using MySQL or MariaDB.  Alternatively, an SQLite database can be
used instead by passing `-s` or `--sqlite` (see 'Options' under 'Usage' to learn
more).

## Usage

This project can be used in the following modes:

- CLI via the `conversed` binary
- GUI (via `conversed viewer` or launched from the ConverseD .desktop file)
- WASM from another application (e.g. the Node.js-based
  [ConverseD Web](https://gitlab.com/aquarichy/conversed-web) front-end)

### CLI commands

``` shell
conversed list-services                    # prints a list of supported services
conversed parse <SERVICE> <CHAT_PATH>      # parses the log at CHAT_PATH and prints it
conversed load <SERVICE> <CHAT_PATH>       # parses the log at CHAT_PATH and loads it into the database
conversed list-chats                       # prints a list of chatrooms found in the database
conversed list-messages-for-chat <CHAT_ID> # prints messages for a given chatroom's ID
conversed viewer                           # launches a GUI viewer
```

- SERVICE: name of service whose logs we are parsing; e.g. 'cicq', 'signal',
  'meta_html', 'meta_json' (Meta: e.g. Facebook or Instagram)
- CHAT PATH: for 'fb', the path to the corresponding messages.html file; for 'cicq', the path to the directory containing
  a cicq chat log.

Options:

- `-s`/`--sqlite`: use an SQLite database (~/.local/shared/conversed/conversed.sqlite) instead of the default MySQL/MariaDB server.
- `--sqlite-path=DB_PATH`: use an SQLite database (at DB_PATH) instead of the default MySQL/MariaDB server.

### Command: parse <SERVICE> <CHAT_PATH>

``` shell
$ conversed parse meta_json test_data/meta_json.2024-02.ag_cm/message_1.json
v Chat 'Catra (catra_123456789012345)' with {Person { name: "Catra", username: "", email: "" }, Person { name: "Adora", username: "", email: "" }}
2024-01-01 05:08:35.936 UTC: Adora:
Happy new year, Catra.
2024-01-01 09:04:13.290 UTC: Catra:
Happy new year!!
2024-01-01 09:04:38.882 UTC: Adora:
Have a cookie!
2024-01-01 09:04:43.054 UTC: Adora:

  IMG[src=your_activity_across_facebook/messages/inbox/catra_123456789012345/photos/123456789_123456789012345_1234567890123456789_n_1234567890123456.jpg]
2024-01-01 21:07:20.168 UTC: Catra:
Reacted ❤️ to your message
2024-01-28 21:15:58.259 UTC: Adora:
🔥
2024-02-23 03:34:31.307 UTC: Adora:
Your travel pics are so good, even from the earlier stories.  I adore the strong poses amid immense vistas.
2024-02-23 05:35:55.216 UTC: Catra:
Hehe thank you ! 💘
^ Chat with {Person { name: "Catra", username: "", email: "" }, Person { name: "Adora", username: "", email: "" }}
```

### Command: load <SERVICE> <CHAT_PATH>

``` shell
$ conversed load meta_json test_data/meta_json.2024-02.ag_cm/message_1.json
Chat log loaded.     8 added,     0 dups,     0 errs.  Service 'meta_json'  for chatroom 'Catra (catra_123456789012345)'
```

### Command: list-chats

``` shell
$ conversed list-chats
 2876  meta_json    Catra (catra_123456789012345)
 2875  cicq         Kim (hungerofthekimpines, kpine@sbb.test)
```

### Command: list-messages-for-chat <CHAT_ID>

``` shell
$ conversed list-messages-for-chat 2876
Connected
2024-02-23 05:35:55 UTC                  Catra: Hehe thank you ! 💘
2024-02-23 03:34:31 UTC                  Adora: Your travel pics are so good, even from the earlier stories.  I adore the strong poses amid immense vistas.
2024-01-28 21:15:58 UTC                  Adora: 🔥
2024-01-01 21:07:20 UTC                  Catra: Reacted ❤️ to your message
2024-01-01 09:04:43 UTC                  Adora:
2024-01-01 09:04:38 UTC                  Adora: Have a cookie!
2024-01-01 09:04:13 UTC                  Catra: Happy new year!!
2024-01-01 05:08:35 UTC                  Adora: Happy new year, Catra.
```

### Command: viewer

``` shell
$ conversed viewer
```

This will bring up the following GTK4-based viewer:

![Viewer](doc/viewer.png)

- Sort: If you have many chats, you can sort them in ascending or descending
  order by clicking the "Chats" button in the top left.  (Note: chats starting
  with an alphabetical character precede those that do not for ascending.)
- Filtering: There is also simple filtering available using an entry box in the
  top-left, in case you have very many chats.
- Load: there is now a menu option to load new log files into the database.

## MySQL/MariaDB Database Configuration

Database parameters can be saved to a TOML-formatted filed at
`~/.config/conversed/conversed.conf` with the following format:

``` toml
[database]
db_name = "conversed"
db_user = "conversed_admin"
db_host = "localhost"
db_port = 3306
db_pass = "YOUR_PASSWORD_HERE"
```

If the file does not exist, the above values are hard-coded as defaults, except
for db_pass.  If db_pass is not specified in `conversed.conf`, then the older
`~/.config/conversed/pw` file is checked (where its entire contents are treated
as the password).  If that does not exist, then the user is prompted to enter
their password on the command-line (WARNING: password is not currently
obfuscated on the terminal!)  Ultimately, a default is expected and required,
and the program aborts if no password can be determined.

## WASM (partial support)

Some degree of WASM support has been reintroduced.  A WASM library module can be
compiled (via `wasm-pack`) for used with at least Node.js with some limited
functionality.  Old support for generating a WASI WASM module for use with
`wasmtime` is *not* currently available.

The WASM module basically only supports parsing of textual data.  It does *not*
support parsing SQL databases (e.g. Signal's SQLite history) and does *not*
support interacting with the Conversed SQL database directly.  This is because
`sqlx` does not currently build for WASM targets.

A WebAssembly module (WASM) is built using `wasm-pack` with a
`nodejs` target and its .wasm and associated JS/TS files can be found in `pkg/`.
You can look inside `examples/` for examples on how to use it.

A minimal example using JavaScript, ES modules and Node looked like:

``` javascript
import * as fs from "node:fs";
import * as Parser from "pkg/conversed.js";

const data = fs.readFileSync ("messages_1.html", "utf8"); # read chat log data into a string
const chat_log = Parser.parse_chat_log_js ("meta_html", data); # pass chat log data as a string to the WASM, get object back

console.log (chat_log);
```

Adjust paths as necessary.

## Signal parsing

The steps to parse them include:

- use Signal's backup feature to obtain their encrypted backup file
- use a third-party tool of your choice to decrypt it
    - this provides you with a file `database.sqlite` in the root of the decrypted backup file tree
    - this can be parsed directly using the `signal_sqlite` service parser
    - e.g. `conversed parse signal_sqlite database.sqlite`

Signal logs can also be parsed indirectly using a custom JSON extract.
This was useful if you wanted to work on string data in memory, or
to support Signal parsing with WASM by bypassing crates like `rusqlite`
and `sqlite` that prevented a WASM from being built.

The additional steps to generate the custom JSON file and parse it are:

- use `scripts/signal_export_to_json.sh` to extract relevant thread and message
  data into a JSON file, e.g. output to `database.json`
- use the `signal_json` service parser on that file
    - e.g. `conversed parse signal_json database.json`

NOTE: some alternative SQLite parsing logic exists using crates other than
`sqlx` in older branches for early experimentation purposes.  Also, some work
was done using `wasi-sdk` to compile a WASM for use with `wasmtime` with a
`wasm32-wasi` target.  The path forward, though, in the near term, will likely
be to expand this project to act as the front-end, rather than Node.js-based
`ConverseD`, and this project will directly populate our Conversed database,
bypassing the need for WASM.  As this project is largely for experimenting
with technology, WASM will liekly continue to be supported to some degree.

## Future Work

- Flatpak for easy distribution
  - (preliminary support complete)
  - get onto Flathub :)
- WASI WASM support
- Meson build system?
- user mapping support (instead of by-chatroom, see by-user, at least for one-on-one)
- configuration file support for SQLite vs. MariaDB
- image support
- more parsers! (MMS, aMSN, IRC, MSN Messenger, etc)

Please view the 'NEWS' file to see what has been completed.

## Acknowledgements

Special thanks to Tyler Woodward for listening to me rant about this project and
providing invaluable feedback.

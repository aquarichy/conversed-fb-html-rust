fn main () {
    #[cfg(feature = "gui")]
    glib_build_tools::compile_resources (
        &["src/ui"],
        "src/ui/resources.gresource.xml",
        "ui.gresource"
    );
}

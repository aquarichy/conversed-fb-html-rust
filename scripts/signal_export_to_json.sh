#!/bin/env bash

# signal_export_to_json.sh -- extracts thread and message information for parsing
#
# Queries a Signal SQLite database file and outputs thread and message information
# as JSON for Conversed's JSON-based Signal parser.
#
# Signal SQLite databases can be obtained through Signal's built-in
# backup feature, which generates an encrypted backup.
# A separate tool will be necessary to decrypt the backup (e.g.
#   https://github.com/mossblaser/signal_for_android_decryption.git
# Disclaimer: I did not write 'signal_for_android_decryption' and
# can make no guarantees about it.)
#
# Once decrypted, there should be a 'database.sqlite' file in the
# root of the backup folder.  That file is provided as an argument
# to this script, which prints (to stdout) the exported JSON.
#
# You can redirect that JSON into a file (e.g. database.json),
# and then let run it directly with conversed-partners-rust or
# with conversed.
#
#  $ signal_export_to_json.sh database.sqlite > database.json
#  $ conversed-partners-rust signal Me database.json
#
# NOTE: there are branches for conversed-partners-rust to
# parse the SQLite database directly using either the `rusqlite`
# or `sqlite` crates.  However, that won't work with the
# current Node.js-based `Conversed` front-end that
# uses.

function abort {
    echo "ERROR: $1" >&2;
    exit 1;
}

function print_usage {
    cat <<EOF
$(basename "$0") <DB_FILE>

  DB_FILE: Signal SQLite Database file, usually named 'database.sqlite'

Outputs a JSON file from a Signal DB containing thread and message information
for parsing by Conversed.
EOF
}

if [ "$1" == "--help" ] || [ "$1" == "-h" ]; then
    print_usage;
    exit 0;
fi

FILE=$1

if ! [ -f "${FILE}" ]; then
    abort "Signal SQLite database file '${FILE}' not found.";
fi

echo -n '{
  "threads": ';

sqlite3 "${FILE}" ".mode json" "
 SELECT DISTINCT
        message.thread_id,

        IIF(groups.title<>'',groups.title,
          IIF(recipient.system_joined_name<>'',recipient.system_joined_name,
            IIF(recipient.profile_joined_name<>'',recipient.profile_joined_name,
              IIF(recipient.username<>'',recipient.username,
                IIF(recipient.e164<>'',recipient.e164,
                  IIF(recipient.aci<>'',recipient.aci,recipient.group_id)
                )
              )
            )
          )
        ) AS chat_name
   FROM message
  INNER JOIN thread    ON message.thread_id   = thread._id
  INNER JOIN recipient ON thread.recipient_id = recipient._id
   LEFT JOIN groups    ON recipient.group_id  = groups.group_id
  GROUP BY message.thread_id;" || abort "Failed to export thread information";

echo -n ',
  "messages": ';

sqlite3 "${FILE}" ".mode json" "
 SELECT message.thread_id,
        message.from_recipient_id,

        /* for Person.name */
        IIF(recipient.system_joined_name<>'',recipient.system_joined_name,
          IIF(recipient.profile_joined_name<>'',recipient.profile_joined_name,
            IIF(recipient.username<>'',recipient.username,
              IIF(recipient.e164<>'',recipient.e164,recipient.aci)
            )
          )
        ) AS name,

        IIF(recipient.e164<>'',recipient.e164,
          IIF(recipient.username<>'',recipient.username,'')
        ) AS email_substitute, /* Signal doesn't use e-mail addresses, but e164 (phone #s) are a comparable identifier; ironically, usernames are an alternate here */

        IIF(recipient.aci<>'',recipient.aci,
          IIF(recipient.group_id<>'',recipient.group_id,'' || recipient._id)
        ) AS username_sysid, /* ironically, not using recipient.username for username as 'aci' is consistent and 'username' is rarely present */

        message.date_sent                          AS unixtime,
        COALESCE(message.body,'')                  AS content
   FROM message
  INNER JOIN recipient ON message.from_recipient_id = recipient._id
  ORDER BY thread_id, message._id;
" || abort "Failed to export message information";

echo '
}';

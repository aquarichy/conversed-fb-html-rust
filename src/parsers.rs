use std::collections::BTreeMap;
use crate::{ChatParser, Parse};

pub mod cicq;
pub mod meta_html;
pub mod meta_json;
pub mod signal_json;
#[cfg(feature = "db")]
pub mod signal_sqlite;
pub mod matrix_json;
pub mod sms;

pub struct ChatParsers {
    parsers : BTreeMap<String,fn() -> ChatParser>,
}
impl ChatParsers {
    pub fn new () -> ChatParsers {
        let mut parsers : ChatParsers = ChatParsers { parsers: BTreeMap::new () };
        /* I really wanted to use `HashMap::from ([ (...), ...]);` but Rust hated that with these closures */
        parsers.parsers.insert ("meta_html".to_string (),     || meta_html::new ());
        parsers.parsers.insert ("meta_json".to_string (),     || meta_json::new ());
        parsers.parsers.insert ("cicq".to_string (),          || cicq::new ());
        parsers.parsers.insert ("signal_json".to_string (),   || signal_json::new ());
        #[cfg(feature = "db")]
        parsers.parsers.insert ("signal_sqlite".to_string (), || signal_sqlite::new ());
        parsers.parsers.insert ("matrix_json".to_string (),   || matrix_json::new ());
        parsers.parsers.insert ("sms".to_string (),           || sms::new ());

        return parsers;
    }

    pub fn service_for_filename (self, filepath : &String) -> Option<String> {
        for (parser_name, parser_new_func) in self.parsers {
            let parser = parser_new_func ();
            eprintln!("checking parser {parser_name}...");
            if parser.can_parse (filepath) {
                eprintln!("         parser {parser_name} selected");
                return Some(parser_name);
            }
        }

        return None;
    }

    pub fn from_service<'a> (self, service : &String) -> Result<ChatParser,String> {
        return match self.parsers.get (service.as_str()) {
            Some(p) => Ok (p()),
            None => Err (format!("ERROR: service '{}' not supported.  Available services: {}",
                                 service, self.get_services ().join (", "))),
        }
    }

    fn get_services (self) -> Vec<String> {
        let mut services : Vec<String> = vec!();
        for key in self.parsers.keys () {
            services.push (key.to_string ());
        }
        return services;
    }

    pub fn list_services (self) {
        println!("Supported services:");
        for key in self.parsers.keys () {
            println!("  {key}");
        }
    }
}

use std::process;
use std::fs::File;
use std::io;
use std::io::{Read,Stdin};
use serde::Deserialize;

static CONF_DIR : &str = ".config/conversed";

#[derive(Deserialize,Debug)]
pub struct DatabaseConfig {
    #[serde(default = "default_db_name")]
    pub db_name: String,
    #[serde(default = "default_db_user")]
    pub db_user: String,
    #[serde(default = "default_db_host")]
    pub db_host: String,
    #[serde(default = "default_db_port")]
    pub db_port: u64,
    #[serde(default = "default_db_pass")]
    pub db_pass: String,
}

#[derive(Deserialize,Debug)]
struct Config {
    #[serde(default = "default_database")]
    database: DatabaseConfig,
}

fn default_db_name  () -> String { "conversed".to_string () }
fn default_db_user  () -> String { "conversed_admin".to_string () }
fn default_db_host  () -> String { "localhost".to_string () }
fn default_db_port  () -> u64    { 3306 }
fn default_database () -> DatabaseConfig { toml::from_str::<DatabaseConfig> ("").unwrap () }

fn default_db_pass  () -> String {
    match get_pass_from_pw_file () {
        Ok(p) => p,
        Err(e) => match get_pass_from_prompt (e) {
            Ok(p) => p,
            Err(e) => {
                eprintln!("Failed to obtain DB password. Error: {e}.  Aborting.");
                process::exit(1);
            }
        }
    }
}

fn get_pass_from_prompt (reason : String) -> Result<String,String> {
    println!("Failed to obtain DB password from either conversed.conf or pw files in ~/{CONF_DIR}/.
Reason: {reason}.
Please enter DB password:");
    let mut pass_str : String = String::new ();
    let stdin : Stdin = io::stdin ();

    match stdin.read_line (&mut pass_str) {
        Ok(_n) => Ok(pass_str.trim ().to_string ()),
        Err(e2) => Err(format!("Failed to read password from stdin. Error: {e2}")),
    }
}

fn get_pass_from_pw_file () -> Result<String,String> {
    let home_path : String = match std::env::var ("HOME") {
        Ok(hp) => hp,
        Err(e) => return Err(format!("$HOME not set, could not access ~/{CONF_DIR}/pw. (Error: {e})")),
    };

    let pw_path : String = format!("{home_path}/{CONF_DIR}/pw");
    let mut pw : String = String::new ();

    match File::open (&pw_path) {
        Ok(mut f) => {
            match f.read_to_string (&mut pw) {
                Ok(_num_bytes) => Ok(pw.trim ().to_string ()),
                Err(e) => Err(format!("Failed to read contents of '{pw_path}': {e}")),
            }
        },
        Err(e) => Err(format!("Failed to open file '{pw_path}': {e}")),
    }
}

pub fn get_db_conf () -> Result<DatabaseConfig,(String,DatabaseConfig)> {
    let home_path : String = match std::env::var ("HOME") {
        Ok(hp) => hp,
        Err(e) => {
            return Err((format!("WARNING: Failed to identify $HOME env var value to look up configuration in $HOME/{CONF_DIR}.  Error: {e}\nTrying to use default configuration."),
                        default_database ()));
        }
    };

    let conf_path : String = format!("{home_path}/{CONF_DIR}/conversed.conf");
    let mut conf_str : String = String::new ();
    match File::open (&conf_path) {
        Ok(mut f) => {
            match f.read_to_string (&mut conf_str) {
                Ok(_n) => {},
                Err(e) => return Err((format!("WARNING: Failed to read conf file at '{conf_path}'.  Error: {e}.\nTrying to use default configuration"), default_database ())),
            }
        },
        Err(e) => {
            match e.kind () {
                std::io::ErrorKind::NotFound => return Ok(default_database ()), /* if the file was simply not found, that's okay */
                _k => return Err((format!("WARNING: Failed to open conf file at '{conf_path}'.  Error: {e}.\nTrying to use default configuration."),
                                  default_database ())),
            }
        },
    }

    let conf_table : Config = match toml::from_str (&conf_str) {
        Ok(ct) => ct,
        Err(e) => return Err((format!("WARNING: Failed to parse '{conf_path}' as TOML file.  Error: {e}.\nTrying to use default configuration."),
                              default_database ())),
    };

    return Ok(conf_table.database);
}

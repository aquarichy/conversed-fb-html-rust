use std::time::Duration;
use sqlx::{Error, FromRow, Executor, Row, SqlitePool};
use sqlx::sqlite::{Sqlite, SqlitePoolOptions, SqliteConnectOptions, SqliteRow};
use sqlx::types::chrono::DateTime;
use std::rc::Rc;
use std::{path,fs};

use async_std::task::block_on;

use conversed::ChatMessage;

use super::{ConversedDb, ConversedDbIface, DbChatroom, DbMessage, DbService};

const CREATE_SERVICES_SQL : &str = "
CREATE TABLE IF NOT EXISTS `services` (
       `id`          INTEGER        PRIMARY KEY AUTOINCREMENT,
       `name`        VARCHAR(255)   UNIQUE NOT NULL
)";

const CREATE_CHATROOMS_SQL : &str = "
CREATE TABLE IF NOT EXISTS `chatrooms` (
       `id`          INTEGER        PRIMARY KEY AUTOINCREMENT,
       `service_id`  INTEGER        NOT NULL REFERENCES services(id),
       `name`        VARCHAR(255)   NOT NULL,
       CONSTRAINT unique_chat UNIQUE (service_id, name)
)";

const CREATE_MESSAGES_SQL : &str = "
CREATE TABLE IF NOT EXISTS `messages` (
       `id`          INTEGER        PRIMARY KEY AUTOINCREMENT,
       `service_id`  INTEGER        NOT NULL REFERENCES services(id),
       `chat_id`     INTEGER        NOT NULL REFERENCES chatrooms(id),
       `sender`      VARCHAR(255)   NOT NULL,
       `dt`          DATETIME       NOT NULL,
       `message`     TEXT           NOT NULL,
       `sourcefile`  VARCHAR(255)   NOT NULL,
       CONSTRAINT unique_message UNIQUE (sender, dt, message)
)";

const CREATE_MAPPING_SQL : &str = "
CREATE TABLE IF NOT EXISTS `mapping` (
       `id`              INTEGER      PRIMARY KEY AUTOINCREMENT,
       `orig_service_id` INTEGER      NOT NULL,
       `orig_sender`     VARCHAR(255) NOT NULL,
       `new_sender`      VARCHAR(255) NOT NULL,
       CONSTRAINT unique_origin UNIQUE (orig_service_id, orig_sender)
)";

impl From::<&SqliteRow> for DbService {
    fn from(service_row : &SqliteRow) -> Self {
        DbService { id: service_row.try_get::<i64,&str> ("id").unwrap_or (-1) }
    }
}
impl <'r> FromRow<'r, SqliteRow> for DbService  {
    fn from_row(row: &'r SqliteRow) -> Result<Self, Error> { Ok(row.into ()) }
}

impl From::<&SqliteRow> for DbChatroom {
    fn from(chat_row: &SqliteRow) -> Self {
        return DbChatroom {
            id: chat_row.try_get::<i64,&str> ("id").unwrap_or (-1),
            chat_name: chat_row.try_get::<String,&str> ("c_name").unwrap_or ("ERR".to_string ()),
            service_name: chat_row.try_get::<String,&str> ("s_name").unwrap_or ("ERR".to_string ()),
        };
    }
}
impl From::<SqliteRow> for DbChatroom {
    fn from (row : SqliteRow) -> Self { (&row).into () }
}
impl <'r> FromRow<'r, SqliteRow> for DbChatroom  {
    fn from_row(row: &'r SqliteRow) -> Result<Self, Error> { Ok(row.into ()) }
}

impl From::<&SqliteRow> for DbMessage {
    fn from (row : &SqliteRow) -> Self {
        return DbMessage { sender:  row.try_get::<> ("sender").unwrap_or ("ERR".to_string ()),
                           date:    row.try_get::<> ("dt").unwrap_or (DateTime::from_timestamp (0,0).unwrap ()),
                           message: row.try_get::<> ("message").unwrap_or ("ERR".to_string()) };
    }
}
impl From::<SqliteRow> for DbMessage {
    fn from (row : SqliteRow) -> Self { (&row).into () }
}
impl <'r> FromRow<'r, SqliteRow> for DbMessage  {
    fn from_row(row: &'r SqliteRow) -> Result<Self, Error> { Ok(row.into ()) }
}


#[derive(Clone)]
pub struct ConversedSqliteDb {
    conn_pool: SqlitePool,
}

impl ConversedSqliteDb {
    pub async fn connect (sqlite_path : &Option<String>) -> Result<ConversedDb, sqlx::Error> {
        let file_path : String = match sqlite_path {
            Some(fp) => fp.to_string (),
            None => {
                let home_path : String = std::env::var ("HOME").expect ("Could not determine $HOME env var value");
                let dir_path : String = format!("{home_path}/.local/share/conversed");
                let file_path : String = format!("{dir_path}/conversed.sqlite");
                let dp = path::Path::new (&dir_path);

                if ! dp.exists () {
                    fs::DirBuilder::new ().create (dp).expect ("Uh oh, failed to created ~/.local/share/conversed/ to host SQLite DB");
                }

                file_path
            }
        };

        let options : SqlitePoolOptions = SqlitePoolOptions::new ()
            .acquire_timeout (Duration::new (5, 0));

        let conn_options : SqliteConnectOptions = SqliteConnectOptions::new ()
            .create_if_missing (true)
            .filename (file_path);

        let conn_pool : SqlitePool = options.connect_with (conn_options).await?;


        /* TODO: note: getting chats for an ID that doesn't exist silently fails/seems to succeed? */

        let sqlite_db : ConversedSqliteDb = ConversedSqliteDb { conn_pool };

        for (table, sql) in [   ("services",  CREATE_SERVICES_SQL),
                                ("chatrooms", CREATE_CHATROOMS_SQL),
                                ("messages",  CREATE_MESSAGES_SQL),
                                ("mapping",   CREATE_MAPPING_SQL), ] {
            sqlite_db.conn_pool.execute (sql).await.expect (format!("Failed to create services {table} with SQL '{}'", sql).as_str ());
        }

        let db : ConversedDb = ConversedDb { db: Rc::new (sqlite_db) };
        return Ok(db);
    }
}

impl ConversedDbIface for ConversedSqliteDb {
    fn get_all_chatrooms (&self, query_str : &str) -> Result<Vec<DbChatroom>,sqlx::Error> {
        block_on (sqlx::query_as::<Sqlite,DbChatroom> (query_str)
                  .fetch_all (&self.conn_pool))
    }

    fn get_chatrooms_for_name (&self, query_chat_str : &str, service_id : i64, chat_name : &String) -> Result<Vec<DbChatroom>,sqlx::Error> {
        block_on (sqlx::query_as::<Sqlite,DbChatroom> (query_chat_str)
                  .bind (chat_name)
                  .bind (service_id)
                  .fetch_all (&self.conn_pool))
    }

    fn get_messages_for_chat (&self, query_str : &str, chat_id : i64) -> Result<Vec<DbMessage>,sqlx::Error> {
        block_on (sqlx::query_as::<Sqlite,DbMessage> (query_str)
                  .bind (chat_id)
                  .fetch_all (&self.conn_pool))
    }

    fn get_service_rows (&self, query_service_str : &str, service : &String) -> Result<Vec<DbService>,sqlx::Error> {
        block_on (sqlx::query_as::<Sqlite,DbService> (query_service_str)
                  .bind (service)
                  .fetch_all (&self.conn_pool))
    }

    fn add_service (&self, insert_service_str : &str, service : &String) -> Result<i64,sqlx::Error> {
        let r = block_on (sqlx::query (insert_service_str)
                        .bind (service)
                        .execute (&self.conn_pool))?;
        return Ok(r.last_insert_rowid () as i64);
    }

    fn add_chat (&self, insert_chat_str : &str, service_id : i64, chat_name : &str) -> Result<i64,sqlx::Error> {
        let r = block_on (sqlx::query (insert_chat_str)
                         .bind (service_id)
                         .bind (&chat_name)
                         .execute (&self.conn_pool))?;
        return Ok(r.last_insert_rowid () as i64);
    }

    fn add_message (&self, insert_message_str : &str, service_id : i64, chat_id : i64, msg : ChatMessage, file_path : &str) -> Result<(),Error> {
        let _ = block_on (sqlx::query::<Sqlite> (insert_message_str)
                          .bind(service_id)
                          .bind (chat_id)
                          .bind (msg.sender)
                          .bind (msg.datetime)
                          .bind (msg.content)
                          .bind (file_path)
                          .execute (&self.conn_pool))?;
        return Ok(());
    }
}

use std::time::Duration;
use std::rc::Rc;

use async_std::task::block_on;

use sqlx::pool::PoolOptions;
use sqlx::{Error, FromRow, Pool, Row};
use sqlx::mysql::{MySql, MySqlPoolOptions, MySqlRow};
use sqlx::types::chrono::DateTime;

use conversed::ChatMessage;
use conversed::conf;
use conversed::conf::DatabaseConfig;

use super::{ConversedDb, ConversedDbIface, DbChatroom, DbMessage, DbService};

impl From::<&MySqlRow> for DbService {
    fn from(service_row : &MySqlRow) -> Self {
        DbService { id: service_row.try_get::<i64,&str> ("id").unwrap_or (-1) }
    }
}
impl <'r> FromRow<'r, MySqlRow> for DbService  {
    fn from_row(row: &'r MySqlRow) -> Result<Self, Error> { Ok(row.into ()) }
}

impl From::<&MySqlRow> for DbChatroom {
    fn from(chat_row: &MySqlRow) -> Self {
        return DbChatroom {
            id: chat_row.try_get::<i64,&str> ("id").unwrap_or (-1),
            chat_name: chat_row.try_get::<String,&str> ("c_name").unwrap_or ("ERR".to_string ()),
            service_name: chat_row.try_get::<String,&str> ("s_name").unwrap_or ("ERR".to_string ()),
        };
    }
}
impl From::<MySqlRow> for DbChatroom {
    fn from (row : MySqlRow) -> Self { (&row).into () }
}
impl <'r> FromRow<'r, MySqlRow> for DbChatroom  {
    fn from_row(row: &'r MySqlRow) -> Result<Self, Error> { Ok(row.into ()) }
}

impl From::<&MySqlRow> for DbMessage {
    fn from (row : &MySqlRow) -> Self {
        return DbMessage { sender:  row.try_get::<> ("sender").unwrap_or ("ERR".to_string ()),
                           date:    row.try_get::<> ("dt").unwrap_or (DateTime::from_timestamp (0,0).unwrap ()),
                           message: row.try_get::<> ("message").unwrap_or ("ERR".to_string()) };
    }
}
impl From::<MySqlRow> for DbMessage {
    fn from (row : MySqlRow) -> Self { (&row).into () }
}
impl <'r> FromRow<'r, MySqlRow> for DbMessage  {
    fn from_row(row: &'r MySqlRow) -> Result<Self, Error> { Ok(row.into ()) }
}


#[derive(Clone)]
pub struct ConversedMySqlDb {
    conn_pool: Pool<MySql>,
}

impl ConversedMySqlDb {
    pub async fn connect () -> Result<ConversedDb, sqlx::Error> {
        let db_conf : DatabaseConfig = match conf::get_db_conf () {
            Ok(dc) => dc,
            Err((e,default_dc)) => {
                eprintln!("{e}");
                default_dc
            }
        };

        let connect_str : String = format!("mysql://{}:{}@{}:{}/{}",
                                           db_conf.db_user, db_conf.db_pass, db_conf.db_host, db_conf.db_port, db_conf.db_name);

        let options : PoolOptions<MySql> = MySqlPoolOptions::new ()
            .acquire_timeout (Duration::new (5, 0));
        let conn_pool : Pool<MySql> = options.connect (connect_str.as_str ()).await?;

        let mysql_db : ConversedMySqlDb = ConversedMySqlDb { conn_pool };

        let db : ConversedDb = ConversedDb { db: Rc::new (mysql_db) };

        return Ok(db);
    }
}
impl ConversedDbIface for ConversedMySqlDb {
    fn get_all_chatrooms (&self, query_str : &str) -> Result<Vec<DbChatroom>,sqlx::Error> {
        block_on (sqlx::query_as::<MySql,DbChatroom> (query_str)
                  .fetch_all (&self.conn_pool))
    }

    fn get_chatrooms_for_name (&self, query_chat_str : &str, service_id : i64, chat_name : &String) -> Result<Vec<DbChatroom>,sqlx::Error> {
        block_on (sqlx::query_as::<MySql,DbChatroom> (query_chat_str)
                  .bind (chat_name)
                  .bind (service_id)
                  .fetch_all (&self.conn_pool))
    }

    fn get_messages_for_chat (&self, query_str : &str, chat_id : i64) -> Result<Vec<DbMessage>,sqlx::Error> {
        block_on (sqlx::query_as::<MySql,DbMessage> (query_str)
                  .bind (chat_id)
                  .fetch_all (&self.conn_pool))
    }

    fn get_service_rows (&self, query_service_str : &str, service : &String) -> Result<Vec<DbService>,sqlx::Error> {
        block_on (sqlx::query_as::<MySql,DbService> (query_service_str)
                  .bind (service)
                  .fetch_all (&self.conn_pool))
    }

    fn add_service (&self, insert_service_str : &str, service : &String) -> Result<i64,sqlx::Error> {
        let r = block_on (sqlx::query (insert_service_str)
                          .bind (service)
                          .execute (&self.conn_pool))?;
        return Ok(r.last_insert_id () as i64);
    }

    fn add_chat (&self, insert_chat_str : &str, service_id : i64, chat_name : &str) -> Result<i64,sqlx::Error> {
        let r = block_on (sqlx::query (insert_chat_str)
                          .bind (service_id)
                          .bind (&chat_name)
                          .execute (&self.conn_pool))?;
        return Ok(r.last_insert_id () as i64);
    }

    fn add_message (&self, insert_message_str : &str, service_id : i64, chat_id : i64, msg : ChatMessage, file_path : &str) -> Result<(),Error> {
        let _ = block_on (sqlx::query::<MySql> (insert_message_str)
                          .bind(service_id)
                          .bind (chat_id)
                          .bind (msg.sender)
                          .bind (msg.datetime)
                          .bind (msg.content)
                          .bind (file_path)
                          .execute (&self.conn_pool))?;
        return Ok(());
    }
}

use std::collections::HashMap;
use std::fmt::{Display,Formatter};
use serde::Deserialize;

#[derive(Deserialize,Debug)]
struct ElementRedactedBecause {
    content: HashMap<String,String>, /* redacts: ... */
    origin_server_ts: u64,
    redacts: String,
    room_id: String,
    sender: String,
    r#type: String, /* e.g. "m.room.redaction" */
    unsigned: ElementUnsignedJustAge,
    event_id: String,
    user_id: String,
    age: u64,
}

#[derive(Deserialize,Debug)]
struct ElementUnsignedWithRedaction {
    redacted_by: Option<String>,
    redacted_because: Option<ElementRedactedBecause>,
    age: i64,
}

#[derive(Deserialize,Debug)]
struct ElementUnsignedJustAge {
    age: i64,
}

#[derive(Deserialize,Debug)]
#[serde(untagged)]
enum ElementUnsigned {
    /* Need to split these out into separate Struct types to avoid infinite recursion where Unsigned has Redaction has Unsigned (just age) */
    WithRedaction(ElementUnsignedWithRedaction),
    JustAge(ElementUnsignedJustAge),
}

#[derive(Deserialize,Debug)]
struct ElementMentions {
    user_ids: Option<Vec<String>>,
    /* ... */
}

/* "org.matrix.msc1767.message" */
#[derive(Deserialize,Debug)]
struct ElementMsc1767Message {
    body: String,
    mimetype: String, /* e.g. "text/plain", "text/html" */
}

#[derive(Deserialize,Debug)]
struct ElementInReplyTo {
    event_id: String,
}

#[derive(Deserialize,Debug)]
#[serde(untagged)]
enum ElementRelatesTo {
    WithReplyTo {
        #[serde(rename = "m.in_reply_to")]
        m_in_reply_to: ElementInReplyTo,
    },
    WithEventId {
        event_id: String,
        rel_type: String, /* e.g. "m.reference" */
    }
}

#[derive(Deserialize,Debug)]
struct ElementKey {
    k: String,
    ext: bool,
    key_ops: Vec<String>, /* e.g. [ "encrypt", "decrypt" ] */
    alg: String,
    kty: String,
}

#[derive(Deserialize,Debug)]
#[serde(deny_unknown_fields)]
struct ElementMediaFile {
    iv: String,
    hashes: HashMap<String,String>,
    key: ElementKey,
    url: String,
    v: String, /* e.g. "v2" */
    mimetype: Option<String>,
}

#[derive(Deserialize,Debug)]
#[serde(deny_unknown_fields)]
struct ElementThumbnailInfo {
    size: u64,
    mimetype: String,
    w: u64,
    h: u64,
}

#[derive(Deserialize,Debug)]
struct ElementMsc1767File {
    size: u64,
    mimetype: String,
    name: String,
}

#[derive(Deserialize,Debug)]
#[serde(deny_unknown_fields)]
struct ElementMediaInfo {
    size: Option<u64>,
    mimetype: String, /* e.g. "image/jpeg" */
    w: Option<u64>,
    h: Option<u64>,
    duration: Option<u64>, /* for video/audio */
    thumbnail_info: Option<ElementThumbnailInfo>, /* a subset of MediaInfo */
    thumbnail_file: Option<ElementMediaFile>,
    #[serde(rename = "xyz.amorgan.blurhash")]
    xyz_amorgan_blurhash: Option<String>,
}

#[derive(Deserialize,Debug)]
struct ElementMsc1767Audio {
    duration: u64,
    waveform: Vec<u64>,
}

/* "org.matrix.msc3245.voice" */
#[derive(Deserialize,Debug)]
struct ElementMsc3245Voice {
}

#[derive(Deserialize,Debug)]
struct ElementPollAnswer {
    id: String,
    #[serde(rename = "org.matrix.msc1767.text")]
    msc1767_text: String,
}

#[derive(Deserialize,Debug)]
struct ElementPollQuestion {
    #[serde(rename = "org.matrix.msc1767.text")]
    msc1767_text: String,
    body: Option<String>,
    msgtype: Option<String>, /* "m.text" */
}

#[derive(Deserialize,Debug)]
struct ElementPollStart {
    /* "org.matrix.msc3381.poll.start" */

    answers: Vec<ElementPollAnswer>,
    kind: String, /* e.g. "org.matrix.msc3381.poll.disclosed" */
    max_selections: i64,
    question: ElementPollQuestion,
}

#[derive(Deserialize,Debug)]
struct ElementMember {
    avatar_url: Option<String>,
    displayname: String,
    membership: String,
}

#[derive(Deserialize,Debug)]
struct ElementRoomName  {
    name: String,
}

#[derive(Deserialize,Debug)]
struct ElementAvatar {
    url: String,
}

#[derive(Deserialize,Debug)]
struct ElementTopic {
    topic: String,
 }

#[derive(Deserialize,Debug)]
#[serde(untagged)]
enum ElementPrevContent {
    PrevMember(ElementMember),
    PrevRoomName(ElementRoomName),
    PrevAvatar(ElementAvatar),
    PrevTopic(ElementTopic),
}

#[derive(Deserialize,Debug)]
struct ElementOffer {
    sdp: String,
    r#type: String, /* e.g. "offer" */
}

#[derive(Deserialize,Debug)]
struct ElementCapabilities {
    #[serde(rename = "m.call.transferee")]
    call_transferee: bool,
    #[serde(rename = "m.call.dtmf")]
    call_dtmf: bool,
}

/* org.matrix.msc3077.sdp_stream_metadata */
#[derive(Deserialize,Debug)]
struct ElementMsc3077SdpStreamMetadata {
    purpose: String, /* e.g. "m.usermedia" */
    audio_muted: bool,
    video_muted: bool,
}

#[derive(Deserialize,Debug)]
#[serde(untagged)]
#[serde(deny_unknown_fields)]
enum ElementContent {
    /* m.room.message */
    MessageText {
        msgtype: String, /* "m.text" */
        body: String,

        #[serde(rename = "m.mentions")]
        m_mentions: Option<ElementMentions>,

        #[serde(rename = "org.matrix.msc1767.text")] 
        msc1767_text: Option<String>, /* like body */
        
        #[serde(rename = "org.matrix.msc1767.message")] 
        msg1767_message: Option<Vec<ElementMsc1767Message>>,

        format: Option<String>, /* e.g. "org.matrix.custom.html" */
        formatted_body: Option<String>,
        #[serde(rename = "m.relates_to")]
        m_relates_to: Option<ElementRelatesTo>,
    },
    MessageMedia {
        msgtype: String, /* "m.image", "m.video", etc */
        body: String, /* e.g. "img_1234.jpeg" */
        info: ElementMediaInfo, /* { size, mimetype, w(idth), h(eight), thumbnail_file? {...}, xyz.amorgan.blurhash?  */
        file: ElementMediaFile, /* { iv, hashes { sha256, ... }, key { k, ext, key_opts[str], alg, kty }, url, v } */
        #[serde(rename = "m.relates_to")]
        m_relates_to: Option<ElementRelatesTo>,
        #[serde(rename = "m.mentions")]
        m_mentions: Option<ElementMentions>,
    },
    MessageVoice {
        msgtype: String, /* "m.audio" */
        body: String,
        #[serde(rename = "org.matrix.msc3245.voice")]
        msc3245_voice: ElementMsc3245Voice,
        #[serde(rename = "org.matrix.msc1767.audio")]
        msc1767_audio: ElementMsc1767Audio, /* duration:u64, waveform:Vec<u64> */
        #[serde(rename = "org.matrix.msc1767.file")]
        msc1767_file: Option<ElementMsc1767File>,
        #[serde(rename = "org.matrix.msc1767.text")]
        msc1767_text: Option<String>,
        info: ElementMediaInfo, /* duration:u64, mimetype:String, size:u64 */
        file: ElementMediaFile,
    },
    MessageVerificationRequest {
        msgtype: String, /* "m.key.verification.request" */
        to: String,
        from_device: String,
        body: String,
        methods: Vec<String>,
        timestamp: Option<u64>,
    },
    MessageOther {
        msgtype: String,
        body: String,
    },
    /*
       org.matrix.msc1767.audio        0.001
       org.matrix.msc3245.voice        0.001
       org.matrix.msc1767.file         0.000
     */

//    ElementMessageContent,
//    Message {
//        msgtype: String, /* e.g. "m.text" */
//        body: String,
//        format: Option<String>, /* e.g. "org.matrix.custom.html" */
//        formatted_body: Option<String>,
//        #[serde(rename = "m.mentions")]
//        m_mentions: Option<ElementMentions>,
//        #[serde(rename = "org.matrix.msc1767.text")] 
//        msc1767_text: Option<String>, /* like body */
//    },
    /* m.room.member */
    Member(ElementMember),
    /* m.call.invite */
    CallInvite {
        call_id: String,
        lifetime: u64,
        offer: ElementOffer,
        party_id: String,
        version: String, /* e.g. "1" */
        capabilities: Option<ElementCapabilities>,
        #[serde(rename = "org.matrix.msc3077.sdp_stream_metadata")]
        msc3077_sdp_stream_metadata: Option<HashMap<String,ElementMsc3077SdpStreamMetadata>>,
    },
    /* m.room.join_rules */
    JoinRules {
        join_rule: String,
    },
    /* "m.room.history_visibility" */
    HistoryVisibility {
        history_visibility: String, /* e.g. "shared" */
    },
    /* "m.room.guest_access" */
    GuestAccess {
        guest_access: String, /* e.g. "can_join" */
    },
    /* m.room.encryption */
    Encryption {
        algorithm: String, /* e.g. "m.megolm.v1.aes-sha2" */
    },
    /* "m.room.name" */
    RoomName(ElementRoomName),
    /* "m.room.topic" */
    Topic(ElementTopic),
    /* "m.room.avatar" */
    Avatar(ElementAvatar),
    /* "org.matrix.msc3381.poll.start" */
    PollStart {
        body: Option<String>,
        #[serde(rename = "org.matrix.msc1767.text")] 
        msc1767_text: Option<String>, /* like body */
        
        #[serde(rename = "org.matrix.msc3381.poll.start")]
        msc3381_poll_start: ElementPollStart,
    },
    PollEnd {
        /* "org.matrix.msc3381.poll.end" */
        body: String,
        #[serde(rename = "org.matrix.msc1767.text")]
        msc1767_text: String,
        #[serde(rename = "m.relates_to")]
        m_relates_to: ElementRelatesTo,
    },
    EmptyMessage {
    }
}

/*
impl Display for ElementRoomMemberContent {
    fn fmt (&self, f : &mut Formatter) -> std::fmt::Result {
        return write!(f, "{} ({}, {})", self.avatar_url, self.displayname, self.membership);
    }
}
*/

impl Display for ElementContent {
    fn fmt (&self, f : &mut Formatter) -> std::fmt::Result {
        return write!(f, "{:?}", &self);
    }
}

#[derive(Deserialize,Debug)]
#[serde(deny_unknown_fields)]
struct ElementMessage {
    content: ElementContent,
    origin_server_ts: i64,
    room_id: String,
    sender: String,
    state_key: Option<String>,
    r#type: String,
    unsigned: ElementUnsigned,
    event_id: String,
    user_id: Option<String>,
    age: Option<i64>,
    redacted_because: Option<ElementRedactedBecause>,
    replaces_state: Option<String>,
    prev_content: Option<ElementPrevContent>,
}

#[derive(Deserialize)]
struct ElementLog {
    room_name: String,
    room_creator: String,
    topic: String,
    export_date: String,   /* day.month.year, e.g. 25.2.2024 */
    exported_by: String,
    messages: Vec<ElementMessage>,
}

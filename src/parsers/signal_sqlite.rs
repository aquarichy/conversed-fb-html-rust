use std::collections::{HashMap, HashSet};
use std::fs;
use std::path::PathBuf;
use crate::{ChatParser, ChatLog, ChatMessage, Parse, Person};
use async_std::task::block_on;
use regex::Regex;
use sqlx::Pool;
use sqlx::sqlite::{Sqlite,SqlitePool};
use sqlx::Row;
use chrono::{DateTime,Utc};

struct SignalSqliteParser {
}

#[derive(Debug)]
struct SignalMessageMeta {
    thread_id: i64,
    from_recipient_id: i64,
    email: String,
    username: String,
}

impl Parse for SignalSqliteParser {
    fn can_parse (&self, file: &String) -> bool {
        let re_str : &str = "(^|/)database.sqlite$";
        let re : Regex = match Regex::new (re_str) {
            Ok(re) => re,
            Err(e) => { eprintln!("Error compiling regex: {e}. re_str: '{re_str}'"); return false },
        };

        return re.is_match (file);
    }

    fn parse (&self, file: &String) -> Result<Vec<crate::ChatLog>,String> {
        /* get connection pool */
        let abs_path : PathBuf = match fs::canonicalize (file) {
            Ok(p) => p,
            Err(e) => return Err(format!("Failed to canonicalize SQLite DB path '${file}'.  Error: {e}")),
        };
        let url : String = format!("sqlite://{}?mode=ro", abs_path.as_os_str ().to_os_string ().to_str ().expect ("Failed to canonicalize path and convert OsString to String"));

        let conn_pool : Pool<Sqlite> = match block_on (SqlitePool::connect (url.as_str ())) {
            Ok(p) => p,
            Err(e) => return Err(format!("Failed to connect to SQLite database.  Error: {e}")),
        };

        let mut chat_logs : Vec<ChatLog> = vec!();
        let mut chat_log_map : HashMap<i64,ChatLog> = HashMap::new ();
        let mut recipient_map : HashMap<i64,Person> = HashMap::new ();

        let query_threads : &str = "
 SELECT DISTINCT
        message.thread_id,
        thread.type AS thread_type, /* 0: individual, 2: group (text_secure), 3: group (signal group v2) */
        recipient._id AS recipient_id,

        /* for ChatLog.name */
        IIF(groups.title<>'',groups.title,
          IIF(recipient.system_joined_name<>'',recipient.system_joined_name,
            IIF(recipient.profile_joined_name<>'',recipient.profile_joined_name,
              IIF(recipient.username<>'',recipient.username,
                IIF(recipient.e164<>'',recipient.e164,
                  IIF(recipient.aci<>'',recipient.aci,recipient.group_id)
                )
              )
            )
          )
        ) AS chat_name
   FROM message
  INNER JOIN thread    ON message.thread_id   = thread._id
  INNER JOIN recipient ON thread.recipient_id = recipient._id
   LEFT JOIN groups    ON recipient.group_id  = groups.group_id
  GROUP BY message.thread_id;";

        /* TODO: eventually include attachment.content_type, attachment.file_name, etc */
        /* TODO: why are some message.body values NULL? */
        let query_messages : &str = "
 SELECT message.thread_id,
        message.from_recipient_id,

        /* for Person.name */
        IIF(recipient.system_joined_name<>'',recipient.system_joined_name,
          IIF(recipient.profile_joined_name<>'',recipient.profile_joined_name,
            IIF(recipient.username<>'',recipient.username,
              IIF(recipient.e164<>'',recipient.e164,recipient.aci)
            )
          )
        ) AS name,

        IIF(recipient.e164<>'',recipient.e164,
          IIF(recipient.username<>'',recipient.username,'')
        ) AS email_substitute, /* Signal doesn't use e-mail addresses, but e164 (phone #s) are a comparable identifier; ironically, usernames are an alternate here */

        IIF(recipient.aci<>'',recipient.aci,
          IIF(recipient.group_id<>'',recipient.group_id,'' || recipient._id)
        ) AS username_sysid, /* ironically, not using recipient.username for username as 'aci' is consistent and 'username' is rarely present */

        COALESCE(recipient.system_joined_name,'')  AS system_joined_name,
        COALESCE(recipient.profile_joined_name,'') AS profile_joined_name,
        COALESCE(recipient.username,'')            AS username,
        COALESCE(recipient.e164,'')                AS e164,
        COALESCE(recipient.aci,'')                 AS aci,

        message.date_sent                          AS unixtime,
        COALESCE(message.body,'')                  AS content
   FROM message
  INNER JOIN recipient ON message.from_recipient_id = recipient._id
  ORDER BY thread_id, message._id;
";

        match block_on (sqlx::query::<Sqlite> (query_threads).fetch_all (&conn_pool)) {
            Ok(rows) => {
                for row in rows {
                    let thread_id : i64 = row.get::<i64,&str> ("thread_id");
                    let chat_log = ChatLog { messages: vec!(),
                                             name:     row.get::<String,&str> ("chat_name"),
                                             people:   HashSet::new () };
                    chat_log_map.insert (thread_id, chat_log);
                    
                }
            },
            Err(e) => return Err(e.to_string ()),
        }

        /* Parse messages and store them in their chat log */
        match block_on (sqlx::query::<Sqlite> (query_messages).fetch_all (&conn_pool)) {
            Ok(rows) => {
                for row in rows {
                    let unixtime  : i64    = row.get::<i64,&str>    ("unixtime");
                    let datetime : DateTime<Utc> = DateTime::from_timestamp (unixtime / 1000, (unixtime % 1000 * 1000000) as u32).unwrap ();

                    let msg_meta : SignalMessageMeta = SignalMessageMeta {
                        thread_id: row.get::<i64,&str>    ("thread_id"),
                        from_recipient_id: row.get::<i64,&str>  ("from_recipient_id"),
                        email: row.get::<String,&str> ("email_substitute"),
                        username: row.get::<String,&str> ("username_sysid"),
                    };

                    let msg : ChatMessage = ChatMessage {
                        sender: row.get::<String,&str> ("name"),
                        content: row.get::<String,&str> ("content"),
                        datetime,
                        media: vec!()
                    };

                    match chat_log_map.get_mut (&msg_meta.thread_id) {
                        Some(c) => {
                            let person : &Person = recipient_map.entry (msg_meta.from_recipient_id).or_insert_with (|| {
                                Person { name: msg.sender.clone (), username: msg_meta.username, email: msg_meta.email }
                            });

                            if ! c.people.contains (person) {
                                c.people.insert (person.clone ());
                            }

                            c.messages.push (msg);
                        },
                        None => {
                            return Err(format!("ERROR: found message with unknown thread_id {}.  Message: {:?}", msg_meta.thread_id, msg));
                        }
                    }
                }
            },
            Err(e) => return Err(e.to_string ()),
        }
        
        /* populate our ChatLog vector from our map */
        for (_, chat_log) in chat_log_map.drain () {
            chat_logs.push (chat_log);
        }

        return Ok(chat_logs);
    }

    fn parse_data (&self, _data : String) -> Result<Vec<crate::ChatLog>,String> {
        dbg!("DBG: TODO implement parse_data");
        Err("DBG: TODO implement parse_data".to_string ())
    }
}

pub fn new () -> ChatParser {
    return ChatParser { service: "signal".to_string (), format: "sqlite".to_string (), parser: Box::new (SignalSqliteParser {})};
}

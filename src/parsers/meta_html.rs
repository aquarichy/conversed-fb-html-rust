use crate::{ChatParser,ChatLog,ChatMessage,Parse,Person};
use crate::{utils,log,xmlutils,report_err};
use crate::xmlutils::{PathPart, node_dbg_child_list};

use std::fs;
use std::collections::HashSet;
use roxmltree::Node;
use roxmltree::ParsingOptions;
use chrono::{NaiveDateTime, DateTime, TimeZone, Utc, LocalResult, FixedOffset};
use regex::Regex;

/**
 * FBM HTML structure:
 *
 * HTML
 * - HEAD
 * - BODY
 *   - DIV.clearfix._ikh
 *     - DIV._4bl9
 *       - DIV._li
 *         - DIV
 *           - TABLE: contains fb link+logo, my profile pic+link+name, index.html+"HOME" link
 *         - DIV._a705
 *           - DIV._3-8y._3-95._a709:   possibly for conversation partner, has name and an image (their profile pic?
 *             - DIV._a70c
 *               - IMG                  other person's profile picture?
 *             - DIV._a70d
 *               - DIV._a70e            contains TEXT of other person's name
 *           - DIV._a706[role="main"]   has links to other parts, has WordEffects and the message history
 *             * DIV._7s7q                            1+ groups of links  to other sections
 *               * A._42ft._4jy0._4jy3._517h._51sy      1+ links to other sections; current one has class 'selected'
 *             * DIV._3-95._a6-g          Message[], or "Word effects"
 *               - DIV._2ph_._a6-h._a6-i    Sender name, or "Word effects"
 *               - DIV._2ph_._a6-p          Content
 *               - DIV._3-94._a6-o
 *                 - DIV._a72d                Date  ("Nov 11, 2020, 1:22:33am"), ugh, not iso :(
 *
 * IG HTML structure is similar, with some variations in class names.
 */


pub fn new () -> ChatParser {
    return ChatParser { service: "meta".to_string (), format: "html".to_string (), parser: Box::new (MetaHtmlChatParser {}) };
}

fn person_set_join_names (set : &HashSet<Person>) -> String {
    let mut v : Vec<String> = Vec::new ();
    for p in set {
        v.push (p.name.clone ());
    }
    return v.join (", ");
}

struct MetaHtmlChatParser {
}

struct PathSet<'a> {
    name:           &'a str,
    test:           Vec<PathPart<'a,'a>>,
    chat_log_container: Vec<PathPart<'a,'a>>,
    chat_log:       Vec<PathPart<'a,'a>>,
    friend:         PathPart<'a,'a>,
    friend_name:    Vec<PathPart<'a,'a>>,
    messages:       PathPart<'a,'a>,
    msg_cls_part:   &'a str,
    msg_sender:     Vec<PathPart<'a,'a>>,
    msg_content:    Vec<PathPart<'a,'a>>,
    msg_date:       Vec<PathPart<'a,'a>>,
    msg_date_format: &'a str,
    generated:      Vec<PathPart<'a,'a>>,
    generated_date_format: &'a str,
}

impl Parse for MetaHtmlChatParser {
    fn can_parse (&self, file: &String) -> bool {
        let re_str : &str = r"(^|/)message_[0-9]+\.html$";
        let re : Regex = match Regex::new (re_str) {
            Ok(re) => re,
            Err(e) => { eprintln!("Error compiling regex: {e}. re_str: '{re_str}'"); return false }
        };

        return re.is_match (file);
    }

    fn parse (&self, file : &String) -> Result<Vec<ChatLog>,String> {
        let data : String = match fs::read_to_string (&file) {
            Ok(d) => d,
            Err(e) => {
                let msg : String = format!("Uh oh, couldn't parse chat log: {e}");
                log(msg.as_str ());
                return Err(msg);
            },
        };

        return self.parse_data (data);
    }

    fn parse_data (&self, mut data : String) -> Result<Vec<ChatLog>,String> {
        utils::set_panic_hook ();

        let chat_log_container : Vec<PathPart> = Vec::from([("body", "*"),
                                                            ("div","clearfix _ikh"),
                                                            ("div", "_4bl9"),
                                                            ("div", "_li")]);
        let friend_fb      : PathPart = ("div", "_3-8y _3-95 _a709");
        let friend_fb_2020 : PathPart = ("div", "_3-8y _3-95 _3b0b");
        let friend_ig      : PathPart = ("div", "_3-8y _3-95 _a70a");

        /* TODO: find a way to define this in an external, user-editable file to avoid recompilation each time :).
           Should switch to CSS Selectors or XPath */
        let path_sets : [PathSet; 3] = [
            PathSet {
                name:           "fb", /* 2022, 2024 */
                test:           { let mut v = chat_log_container.clone (); v.extend ([("div", "_a705"), friend_fb]); v },
                chat_log_container: chat_log_container.clone (),
                chat_log:       Vec::from([("div", "_a705")]),
                friend:         friend_fb, /* fb */
                friend_name:    Vec::from([("div", "_a70d"), ("div", "_a70e")]),
                messages:       ("div", "_a706"),
                msg_cls_part:   "_a6-g",
                msg_sender:     Vec::from([("div", "_2ph_ _a6-h _a6-i")]), /* fb */
                msg_content:    Vec::from([("div", "_2ph_ _a6-p")]), /* fb */
                msg_date:       Vec::from([("div", "_3-94 _a6-o"), /* fb */
                                           ("div", "_a72d")]),
                /* https://docs.rs/chrono/latest/chrono/format/strftime/index.html#specifiers */
                msg_date_format: "%b %e, %Y %l:%M:%S%P",
                generated:      Vec::from([("div","_a707"),("div","")]),
                /* E.g. "Friday, November 11, 2022 at 5:53 AM UTC-06:00" */
                generated_date_format: "%A, %B %-d, %Y at %-l:%M %p UTC%:z",
            },
            PathSet {
                name:           "fb-2020",
                test:           { let mut v = chat_log_container.clone (); v.extend ([("div", "_3a_u"), friend_fb_2020]); v },
                chat_log_container: chat_log_container.clone (),
                chat_log:       Vec::from([("div", "_3a_u")]),
                friend:         friend_fb_2020,
                friend_name:    Vec::from([("div", "_3b0c"), ("div", "_3b0d")]),
                messages:       ("div", "_4t5n"),
                msg_cls_part:   "_3-95 _2pi0 _2lej",
                msg_sender:     Vec::from([("div", "_3-96 _2pio _2lek _2lel")]),
                msg_content:    Vec::from([("div", "_3-96 _2let")]),
                msg_date:       Vec::from([("div", "_3-94 _2lem")]),
                msg_date_format: "%b %e, %Y, %l:%M %P",
                generated:      Vec::from([("div","_4t5o"), ("div","")]),
                /* E.g. "Monday, May 4, 2020 at 2:45 AM UTC-04:00" */
                generated_date_format: "%A, %B %-d, %Y at %-l:%M %p UTC%:z",
            },
            PathSet {
                name:           "ig",
                test:           { let mut v = chat_log_container.clone (); v.extend ([("div", "_a705"), friend_ig]); v },
                chat_log_container: chat_log_container.clone (),
                chat_log:       Vec::from([("div", "_a705")]),
                friend:         friend_ig, /* ig */
                friend_name:    Vec::from([("div", "_a70d"), ("div", "_a70e")]),
                messages:       ("div", "_a706"),
                msg_cls_part:   "_a6-g",
                msg_sender:     Vec::from([("div", "_3-95 _2pim _a6-h _a6-i")]), /* ig */
                msg_content:    Vec::from([("div", "_3-95 _a6-p")]), /* ig */
                msg_date:       Vec::from([("div", "_3-94 _a6-o")]), /* ig */ /* TODO make sure ("","") gets treated as a terminator/no-op */
                msg_date_format: "%b %e, %Y, %l:%M %P",
                generated:      Vec::from([("div","_a707"),("div","")]),
                generated_date_format: "%A, %B %-d, %Y at %-l:%M %p UTC%:z",
            }
        ];

        /* TODO: rework this; I dislike having hard-coded variables for FB vs IG;
        either identify the common classes we need, and do a more intelligent check to see
        whether a node contains that class (rather than just == as we do now), or have
        competing profile structs for fb vs ig.  At some point, these class names will likely change,
        so we could end up supporting a lot of variants... people should just use JSON probably. :) */

        data.insert_str (0, "<!DOCTYPE htmlrs [
  <!ENTITY nbsp ' '>
]>");

        let doc : roxmltree::Document = match roxmltree::Document::parse_with_options (&data, ParsingOptions { allow_dtd: true, nodes_limit: u32::MAX }) {
            Ok(d) => d,
            Err(e) => return report_err (format!("Failed to parse document file: {e}.  Document data (may be large or empty):\n{}", data.as_str ()))
        };

        let root : roxmltree::Node = doc.root_element ();
        assert_eq!(root.tag_name ().name (), "html");

        let mut path_set_opt : Option<PathSet> = None;

        for path_set in path_sets {
            if xmlutils::node_get_desc_from_path (&root, &path_set.test).is_some () {
                path_set_opt = Some(path_set);
                break;
            }
        };

        let path_set = match path_set_opt {
            Some(s) => s,
            None => return report_err (format!("Failed to identify Meta HTML version.  You may need to define a new PathSet for this in meta_html.rs"))
        };

        let node : roxmltree::Node = match xmlutils::node_get_child_with (&root, &("body", "*")) {
            Some(n) => n,
            None => return report_err (format!("Failed to find <body> tag in document"))
        };
        assert_eq!(node.tag_name ().name (), "body");

        let chat_log_container : roxmltree::Node = match xmlutils::node_get_desc_from_path (&root, &path_set.chat_log_container) {
            Some(n) => n,
            None => return report_err (format!("Failed to find chat log container node at path '{:?}'", &path_set.chat_log_container))
        };

        /* Get TZ for the datetime the log was generated on.  We'll use this as a heuristic to
        guess a likely TZ for when messages in the log were sent, though this is wrong if a user
        sent messages in a different TZ than where they generated the log in. */
        let generated_node : roxmltree::Node = match xmlutils::node_get_desc_from_path (&chat_log_container, &path_set.generated) {
            Some(n) => n,
            None => return report_err (format!("Failed to find 'generated' node at path '{:?}'", &path_set.generated))
        };
        let generated_text : &str = match generated_node.text () {
            Some(t) => t,
            None => return report_err (format!("Failed to obtain text from 'generated' note")),
        };

        let generated_date : &str = match generated_text.rsplit_once (" on ") {
            Some(d) => d.1.trim (),
            None => return report_err (format!("Failed to obtain date portion from 'generated' text, while splitting on ' on '.  Text: {generated_text}")),
        };
        let generated_datetime = match DateTime::parse_from_str (generated_date, path_set.generated_date_format) {
            Ok(dt) => dt,
            Err(e) => return report_err (format!("Failed to obtain 'generated' datetime, error: {}.  generated_date: [{}]. generated_date_format: [{}]", e.to_string (), generated_date, path_set.generated_date_format)),
        };
        let generated_tz : FixedOffset = generated_datetime.timezone ();

        let chat_log_node : roxmltree::Node = match xmlutils::node_get_desc_from_path (&chat_log_container, &path_set.chat_log) {
            Some(n) => n,
            None => return report_err (format!("Failed to find chat log node at path '{:?}'", &path_set.chat_log))
        };
        let friend : Node = match xmlutils::node_get_child_with (&chat_log_node, &path_set.friend) {
            Some(n) => n,
            None => return report_err (format!("Failed to find 'friend' node at path '{:?}' (path set '{}')", path_set.friend, path_set.name))
        };
        let friend_name : &str = match xmlutils::node_get_desc_from_path (&friend, &path_set.friend_name) {
            Some(n) => match n.text () {
                Some(nt) => nt,
                None => { let _ = report_err (format!("Failed to extract text for 'friend_name' node at path '{:?}'", &path_set.friend_name));
                          "[EMPTY FRIEND NAME]" } /* warn, but return "", as people with deleted profiles may have empty friend names :| */
            },
            None => return report_err (format!("Failed to find 'friend_name' node at path '{:?}'", &path_set.friend_name))
        };

        let mut people: HashSet<Person> = HashSet::new ();
        people.insert (Person { name: friend_name.to_string (), username: String::new (), email: String::new () }); /* TODO: derive username from path where possible */

        let messages : Vec<ChatMessage> = vec!();
        let mut chat_log : ChatLog = ChatLog { name: person_set_join_names (&people), people, messages };

        let message_nodes : Node = match xmlutils::node_get_child_with (&chat_log_node, &path_set.messages) {
            Some(ns) => ns,
            None => return report_err (format!("Failed to find 'message_nodes', at path '{:?}'", &path_set.messages))
        };

        for msg in message_nodes.children () {
            if msg.is_text () && msg.text ().unwrap_or ("").trim () == "" {
                /* Skip empty text nodes, present in prettified messages_x.html files */
                continue;
            }
            let cls : &str = match msg.attribute ("class") {
                Some(c) => c,
                None => return report_err (format!("Message node was missing class attribute. msg: {:?}", msg))
            };

            if cls.contains (path_set.msg_cls_part) {
                let mut sender : &str = xmlutils::node_get_text_from_path (&msg, &path_set.msg_sender);
                if sender == "" {
                    let children : String = node_dbg_child_list (msg);
                    let _ = report_err (format!("Failed to find 'sender' for message with path {:?}.  May be the 'Participants' node.  Continuing.  msg's children: {}", &path_set.msg_sender, children));
                    sender = "[EMPTY FRIEND NAME]";
                }

                let content : String = match xmlutils::node_get_desc_from_path (&msg, &path_set.msg_content) {
                    Some(n) => xmlutils::node_inner_text (n),
                    None => String::from ("(none found)"),
                };

                let date_str : &str = xmlutils::node_get_text_from_path (&msg, &path_set.msg_date);

                let mut media : Vec<String> = Vec::new ();
                for img_node in xmlutils::node_find_descs_with (&msg, "img", "*") {
                    media.push (String::from (img_node.attribute ("src").unwrap_or ("")));
                }

                /* Format example: "Sep 30, 2022 9:07:14pm" */
                let datetime : NaiveDateTime = match NaiveDateTime::parse_from_str (date_str, path_set.msg_date_format) {
                    Ok(dt) => dt.into (),
                    Err(e) => {
                        let err_msg = format!("Error parsing date time '{}': {}.  Skipping message. (sender: '{}', content: '{}')", date_str, e, sender, content);
                        log (&err_msg);
                        continue;
                    }
                };

                /* Convert from NaiveDateTime to Local (using TZ of 'Generated' dt) and Utc DateTimes.
                Want Utc for deduplication.  Local will sometimes be useful/accurate, but will be local for
                the timezone that the log was *generated in*, not for when the message was actually sent. So,
                if a user changed timezones since messages were sent, it won't reflect the true local time. :| */
                let local_dt : DateTime<FixedOffset> = match generated_tz.from_local_datetime (&datetime) {
                    LocalResult::Single(d) => d,
                    LocalResult::Ambiguous(d1,d2) => return report_err(format!("Failed to associate tz {:?} with message local datetime {}.  Ambiguous.  d1:{}, d2:{}", generated_tz, datetime, d1, d2)),
                    LocalResult::None => return report_err(format!("Failed to associate tz {:?} with message local datetime {}.  'None' encountered.", generated_tz, datetime)),
                };
                let utc_dt : DateTime<Utc> = local_dt.with_timezone (&Utc);

                let msg : ChatMessage = ChatMessage { sender: String::from (sender), content, datetime: utc_dt, media };
                chat_log.messages.push (msg);
            } else {
                println!("Ignoring 'message' with class {cls}");
            }
        }

        println!("friend: {friend_name}");

        return Ok (vec!(chat_log));
    }
}

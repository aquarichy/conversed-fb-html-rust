#![allow(dead_code)]
// We define a lot of additional fields for Serde to deserialize into, with aspirations to integrate more of them into Conversed

use crate::{ChatParser,ChatLog,Parse,log,ChatMessage,Person};
use std::fs;
use std::collections::HashSet;
use chrono::{DateTime,Utc};
use serde::Deserialize;
use regex::Regex;

pub fn new () -> ChatParser {
    return ChatParser { service: "meta".to_string (), format: "json".to_string (), parser: Box::new (MetaJsonChatParser {}) };
}

#[derive(Debug,Deserialize)]
struct MetaParticipant {
    name: String,
}

#[derive(Debug,Deserialize)]
struct MetaReaction {
    reaction: String,
    actor: String,
}

#[derive(Debug,Deserialize)]
struct MetaPhoto {
    uri: String,
    creation_timestamp: Option<i64>,
}

#[derive(Debug,Deserialize)]
struct MetaMessage {
    sender_name: String,
    timestamp_ms: i64,
    content: Option<String>,
    photos: Option<Vec<MetaPhoto>>,
    reactions: Option<Vec<MetaReaction>>, /* TODO: add support for reactions, also look at Signal/Matrix/FB */
    is_geoblocked_for_viewer: bool,     /* TODO: figure out what I want to do with this */
}

#[derive(Debug,Deserialize)]
struct MetaMagicWord {
    magic_word: String,
    creation_timestamp_ms: i64,
    animation_emoji: String,
}

#[derive(Debug,Deserialize)]
struct MetaJson {
    participants: Vec<MetaParticipant>,
    messages: Vec<MetaMessage>,
    title: String, /* chatroom name: user display name or group */
    thread_path: String, /* e.g. "inbox/username_557100642123456" */
    magic_words: Vec<MetaMagicWord>
}

struct MetaJsonChatParser {
}

/* Dumb fix for dumb problem:
   "ö" (U+00F6) has UTF-8 encoding of "0xC3 0xB6".  For some reason, Meta decided
   to treat UTF-8 hex values as Unicode points!, like "\u00c3\u00b6".
   Except \u00c3 != 0xC3.  It actually == 'Ã'!  And \u00b6 == '¶'!
   So Meta erroneously encodes "ö" as "\u00c3\u00b6" which then gets decoded as "Ã¶"!  Dumb!
*/
fn fix_utf8 (orig : String) -> String {
    return match String::from_utf8 (orig.chars ().map (|c| { c as u8 }).collect::<Vec<u8>> ()) {
        Ok(s) => s,
        Err(e) => {
            log (format!("Uh oh, encountered error '{e}' when fixing strings.  Using original: {orig}").as_str ());
            return orig;
        }
    };
}

impl Parse for MetaJsonChatParser {
    fn can_parse (&self, file: &String) -> bool {
        let re_str : &str = r"(^|/)message_[0-9]+\.json$";
        let re : Regex = match Regex::new (re_str) {
            Ok(re) => re,
            Err(e) => { eprintln!("Error compiling regex: {e}. re_str: '{re_str}'"); return false }
        };

        return re.is_match (file);
    }

    fn parse (&self, file: &String) -> Result<Vec<ChatLog>,String> {
        let data : String = match fs::read_to_string (file) {
            Ok(d) => d,
            Err(e) => {
                let msg : String = format!("Uh oh, couldn't parse chat log: {e}");
                log (msg.as_str ());
                return Err (msg);
            }
        };

        return self.parse_data (data);
    }

    fn parse_data (&self, data: String) -> Result<Vec<ChatLog>,String> {
        let obj : MetaJson = match serde_json::from_str (&data) {
            Ok(o) => o,
            Err(e) => return Err(format!("{}", e))
        };

        /* e.g. "archived_threads/yungniel_12345678" > "yungniel_12345678" */
        let chat_id : &str =  match obj.thread_path.rsplit_once ("/") {
            Some((_dir,chat_id)) => chat_id,
            None => "" /* TODO: consider reporting an error instead */
        };

        let mut people : HashSet<Person> = HashSet::new ();

        for participant in obj.participants {
            let participant_name = if participant.name == "" {
                format!("[UnknownParticipant] ({})", chat_id)
            } else {
                participant.name
            };
            people.insert (Person { name: fix_utf8 (participant_name), username: String::new (), email: String::new () }); /* TODO derive username from path where possible */
        }

        let mut messages : Vec<ChatMessage> = vec!();
        for message in obj.messages {
            let mut media : Vec<String> = vec!();
            match message.photos {
                Some(photos) => {
                    for photo in photos {
                        media.push (photo.uri);
                    }
                },
                None => {}
            }

            let datetime : DateTime<Utc> = match DateTime::from_timestamp (message.timestamp_ms / 1000, (message.timestamp_ms % 1000 * 1000000) as u32) {
                Some(dt) => dt,
                None => {
                    /* TODO: consider not skipping just because of a bad date */
                    let err_msg = format!("Error parsing timestamp for IG message. Skipping message.");
                    log(&err_msg);
                    continue;
                },
            };

            let sender : String = if message.sender_name == "" {
                format!("[UnknownSender] ({chat_id})")
            } else {
                fix_utf8 (message.sender_name)
            };

            messages.push (ChatMessage {
                sender,
                content: fix_utf8 (message.content.unwrap_or ("".to_string ())),
                media,
                datetime,
            });
        }

        return Ok(vec!(ChatLog { name: format!("{} ({})", fix_utf8 (obj.title), chat_id), messages, people }));
    }
}

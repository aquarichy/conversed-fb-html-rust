use std::process;
use std::cmp::Ordering;
use std::rc::Rc;
use std::cell::Cell;
use async_std::task::block_on;
use gtk4::gio::{ActionEntry, Cancellable, MenuModel};
use gtk4::{prelude::*, AlertDialog, ApplicationWindow, FileDialog, MenuButton};
use gtk4::{Application, Builder, Button, CustomFilter, Entry, FilterListModel, ListItem, ListView, SelectionModel, SignalListItemFactory, SingleSelection, Label};
use gtk4::{glib, gio};
use glib::{ExitCode,GString};
use gio::{ListModel, ListStore, File};

use crate::db::{DbChatroom,DbMessage,ConversedDb};

use glib::{Object,clone};

/* Consider using this more widely outside of ui mod */
const APP_ID : &str = "org.kosmokaryote.conversed";

trait SetLabel {
    fn set_label (self, label : &Label);
}

mod chatroom_imp {
    use std::cell::{Cell,RefCell};

    use gtk4::prelude::*;
    use gtk4::subclass::prelude::*;
    use gtk4::glib::{self, Properties};

    #[derive(Default,Properties)]
    #[properties(wrapper_type = super::UiChatroom)]
    pub struct UiChatroomPriv {
        #[property(get,set)]
        pub id: Cell<i64>,
        #[property(get,set)]
        pub chat_name: RefCell<String>,
        #[property(get,set)]
        pub service_name: RefCell<String>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for UiChatroomPriv {
        const NAME : &'static str = "ConversedUiChatroom";
        type Type = super::UiChatroom;
    }

    #[glib::derived_properties]
    impl ObjectImpl for UiChatroomPriv {}
}
glib::wrapper! {
    pub struct UiChatroom(ObjectSubclass<chatroom_imp::UiChatroomPriv>);
}
impl UiChatroom {
    pub fn new(chatroom : DbChatroom) -> Self {
        Object::builder ()
            .property ("id", chatroom.id)
            .property ("chat-name", chatroom.chat_name)
            .property ("service-name", chatroom.service_name)
            .build ()
    }
}
impl From::<DbChatroom> for UiChatroom {
    fn from (db_chatroom : DbChatroom) -> Self { UiChatroom::new (db_chatroom) }
}
impl SetLabel for UiChatroom {
    fn set_label (self, label : &Label) {
        let id : i64 = self.property ("id");
        let chat_name : String = self.property ("chat-name");

        label.set_text (format!("{chat_name} ({id})").as_str ());
    }
}

mod message_imp {
    use std::cell::RefCell;

    //use chrono::{DateTime, Utc};
    use gtk4::prelude::*;
    use gtk4::subclass::prelude::*;
    use gtk4::glib::{self, Properties};

    #[derive(Default,Properties)]
    #[properties(wrapper_type = super::UiMessage)]
    pub struct UiMessagePriv {
        #[property(get,set)]
        pub sender: RefCell<String>,
        #[property(get,set)]
        pub date: RefCell<String>,
        #[property(get,set)]
        pub message: RefCell<String>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for UiMessagePriv {
        const NAME : &'static str = "ConversedUiMessage";
        type Type = super::UiMessage;
    }

    #[glib::derived_properties]
    impl ObjectImpl for UiMessagePriv {
    }
}
glib::wrapper! {
    pub struct UiMessage(ObjectSubclass<message_imp::UiMessagePriv>);
}
impl UiMessage {
    pub fn new(msg : DbMessage) -> Self {
        Object::builder ()
            .property ("sender", msg.sender)
            .property ("date", msg.date.to_string ()) // TODO figure out if we can store chrono or glib DateTime
            .property ("message", msg.message)
            .build ()
    }
}
impl From::<DbMessage> for UiMessage {
    fn from (db_message : DbMessage) -> Self { UiMessage::new (db_message) }
}
impl SetLabel for UiMessage {
    fn set_label (self, label : &Label) {
        label.set_text (format!("{}   {:>20}: {}", self.date (), self.sender (), self.message ()).as_str ());
    }
}

fn populate_list_view<A: From::<B> + IsA<Object> + SetLabel,B> (
    list_view : &ListView,
    store : ListModel
) {
    let factory : SignalListItemFactory = SignalListItemFactory::new ();
    factory.connect_setup (|_factory, list_item_obj| {
        let list_item : &ListItem = list_item_obj.downcast_ref::<ListItem> ().unwrap ();
        let label : Label = Label::new (Some (""));
        label.set_xalign (0.0);
        list_item.set_child (Some (&label));
    });
    factory.connect_bind (|_factory, list_item_obj| {
        let list_item : &ListItem = list_item_obj.downcast_ref::<ListItem> ().unwrap ();
        let ui_object : A = list_item.item ().and_downcast::<A> ().unwrap ();
        let label : Label = list_item.child ().and_downcast::<Label> ().unwrap ();
        ui_object.set_label (&label);
    });

    let sel_model : SingleSelection = SingleSelection::new (Some (store));
    list_view.set_model (Some (&sel_model));
    list_view.set_factory (Some (&factory));
}

fn sort_chats (chat_store : &ListStore, chat_sort_asc : bool) {
    chat_store.sort (|obj_a, obj_b| {
        let chat_a : &UiChatroom = obj_a.downcast_ref ().unwrap ();
        let chat_b : &UiChatroom = obj_b.downcast_ref ().unwrap ();
        let name_a : String = chat_a.chat_name ();
        let name_b : String = chat_b.chat_name ();

        let order = if name_a.chars ().next ().unwrap_or ('Z').is_alphabetic () && ! name_b.chars ().next ().unwrap_or ('Z').is_alphabetic () {
            Ordering::Less
        } else {
            chat_a.chat_name ().cmp (&chat_b.chat_name ())
        };

        if chat_sort_asc {
            order
        } else {
            if order == Ordering::Greater {  Ordering::Less } else { Ordering::Greater }
        }
    });
}

fn populate_chats (db : &ConversedDb,
                   chats_list_filter_entry : &Entry,
                   chats_list_button : &Button,
                   chats_list_view: &ListView,
                   messages_list_view : &ListView) -> Result<(),String> {
    let chats : Vec<DbChatroom> = match block_on (db.get_chatrooms ()) {
        Ok(c) => c,
        Err(e) => return Err(format! ("Error loading chats: {}", e.to_string ())),
    };

    let chats_store : ListStore = ListStore::new::<UiChatroom> ();

    for db_chat in chats {
        let ui_chat : UiChatroom = db_chat.into ();
        chats_store.append (&ui_chat);
    }

    /* set up sorting for Chatrooms */
    let chat_sort_asc : Rc<Cell<bool>> = Rc::new (Cell::new (true));

    chats_list_button.connect_clicked (clone!(@strong chats_store => move |_btn| {
        sort_chats (&chats_store, chat_sort_asc.get ());
        chat_sort_asc.set (!chat_sort_asc.get ());
    }));

    /* TODO: for some reason, doing just 'true' (asc) doesn't actually sort records as expected :| */
    sort_chats (&chats_store, false);
    sort_chats (&chats_store, true);

    /* set up filtering for Chatrooms */
    let chat_filter_model : FilterListModel = FilterListModel::new (Some (chats_store), None::<CustomFilter>);

    chats_list_filter_entry.connect_changed (clone!(@strong chat_filter_model => move |entry| {
        let chat_filter : CustomFilter = CustomFilter::new (clone!(@strong entry => move |obj| {
            let filter_text : GString = entry.text ();
            let chatroom : &UiChatroom = obj.downcast_ref::<UiChatroom> ().unwrap ();
            chatroom.chat_name ().contains (filter_text.as_str ())
        }));
        chat_filter_model.set_filter (Some (&chat_filter));
    }));

    /* populate Chat list */
    populate_list_view::<UiChatroom, DbChatroom> (chats_list_view, chat_filter_model.into ());

    chats_list_view.connect_activate (
        clone!(@strong db, @strong messages_list_view => move |chats_list_view, position| {
            /* populate messages for clicked Chat */
            let chats_model : SelectionModel = chats_list_view.model ().expect ("Chats ListView model is missing?!");
            let chatroom : UiChatroom = chats_model.item (position).and_downcast::<UiChatroom> ().expect ("ListItem didn't contain a UiChatroom?!");
            let chatroom_id : i64 = chatroom.id ();

            let messages : Vec<DbMessage> = match block_on (ConversedDb::get_messages_for_chat (&db, chatroom_id)) {
                Ok(c) => c,
                Err(e) => { eprintln!("ERROR: Couldn't load chats for id {}: {}", chatroom_id, e); return; }
            };

            let message_store : ListStore = ListStore::new::<UiMessage> ();

            for db_message in messages {
                let ui_message : UiMessage = db_message.into ();
                message_store.append (&ui_message);
            }

            populate_list_view::<UiMessage, DbMessage> (&messages_list_view, message_store.into ());
        })
    );

    return Ok(());
}

fn show_error (win : &ApplicationWindow, err_msg : String) {
    let dialogue = AlertDialog::builder ()
        .buttons (["OK"])
        .message ("Conversed: Error")
        .detail (err_msg)
        .modal (true)
        .build ();

    dialogue.show (Some (win));
}

fn load_action (win : &ApplicationWindow, db : ConversedDb, chats_store: & ListStore) {
    let file_dialogue : FileDialog = FileDialog::new ();
    file_dialogue.open (Some (win), None::<Cancellable>.as_ref (), clone!(@strong win, @strong chats_store => move |file_res| {
        let file : File = match file_res {
            Ok(f) => f,
            Err(e) => { show_error (&win, format!("Error opening file {e}")); return },
        };

        let file_path : String = match file.path () {
            Some(p) => { p.as_os_str ().to_str ().unwrap ().to_string () },
            None => { show_error (&win, format!("Somehow we selected a file without a path?! file dbg: {:?}", file)); return },
        };

        // TODO: *don't* block on load; freezes UI.
        // TODO: Also, append chats as each one is loaded for multi-chat logs (e.g. Signal's database.sqlite) rather than all at once
        let chats : Vec<DbChatroom> = match block_on (super::load_auto (&file_path, &db)) {
            Ok(c) => c,
            Err(e) => { show_error (&win, e); return },
        };

        for db_chat in chats {
            let ui_chat : UiChatroom = db_chat.into ();
            chats_store.append (&ui_chat);
        }
    }));
}

fn setup_actions (app: &Application, window : &ApplicationWindow, db: &ConversedDb, chats_list_view: &ListView) {
   let chats_store  = chats_list_view
        .model ().unwrap ().downcast::<SingleSelection> ().unwrap ()
        .model ().unwrap ().downcast::<FilterListModel> ().unwrap ()
        .model ().unwrap ().downcast::<ListStore> ().unwrap ();

    let actions : [ActionEntry<ApplicationWindow>; 2] = [
        ActionEntry::builder("load" ).activate (clone!(@strong db, @strong chats_store => move |win, _, _| load_action (win, db.clone (), &chats_store))).build (),
        ActionEntry::builder("quit" ).activate (| win : &ApplicationWindow, _, _| { win.close (); }).build (),
    ];
    window.add_action_entries (actions);

    app.set_accels_for_action ("win.load", &[ "<Ctrl>L" ]);
    app.set_accels_for_action ("win.quit", &[ "<Ctrl>Q" ]);
}

pub fn activate (app: &Application, db : &ConversedDb) {
    let builder : Builder = Builder::from_resource ("/org/kosmokaryote/conversed/window.ui");
    let window : ApplicationWindow = builder.object ("conversed-window").unwrap ();
    window.set_application (Some (app));

    let menu_builder : Builder = Builder::from_resource ("/org/kosmokaryote/conversed/menu.ui");
    let menu_model : MenuModel = menu_builder.object ("title-menu").unwrap ();
    let menu_button : MenuButton = builder.object ("title-menu").unwrap ();
    menu_button.set_menu_model (Some (&menu_model));

    let chats_list_filter_entry : Entry = builder.object ("conversed-chats-filter").unwrap ();
    let chats_list_button : Button = builder.object ("conversed-chats-header-button").unwrap ();
    let chats_list_view : ListView = builder.object ("conversed-chats-listview").unwrap ();
    let messages_list_box : ListView = builder.object ("conversed-messages").unwrap ();

    match populate_chats (&db, &chats_list_filter_entry, &chats_list_button, &chats_list_view, &messages_list_box) {
        Ok(_) => {},
        Err(e) => eprintln!("Error populating chats: {e}"),
    }

    setup_actions (app, &window, db, &chats_list_view);

    window.present ();
}

pub fn run (db : ConversedDb) {
    gio::resources_register_include!("ui.gresource").expect ("Failed to register resources.");

    let app : Application = Application::builder ()
        .application_id (APP_ID)
        .build ();
    app.connect_activate (move |app| { activate (app, &db); });
    let no_args : [&str; 0] = [];
    let exit_code : ExitCode = app.run_with_args (&no_args);
    process::exit (exit_code.into ());
}

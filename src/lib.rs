//! Parse Facebook HTML chat logs
//!
//! Provides structures for a parser, a chatlog and chat messages.
//! For use by products like `conversed`.
//!
//! # Example
//!
//! From rust:
//!
//! ```
//! use conversed_fb_html_parser_prototype;
//!
//! let parser : FbHtmlParser = FbHtmlParser::new ();
//! let log : FbHtmlChatLog = parser.parse ("messages.1.html");
//! log.print_chat_log ();
//! ```
//!
//! From Node.js via wasm:
//!
//! ```
//! import * as fbhtml from "path_to_wasm_pkg/conversed_fb_html_parser_prototype";
//! import * as fs from "node:fs";
//!
//! /* Load chatlog as string, as our Rust WASM module won't have access to the system unless using wasmtime */
//! const chat_log_data_str = fs.readFileSync ("path_to_html/message_1.html", "utf8");
//!
//! const chat_log_obj = fbhtml.parse_chat_log_js (chat_log_data_str);
//! console.log (chat_log_obj);
//! ```

mod utils;    /* for set_panic_hook () for better rustwasm error messages */
mod xmlutils;
pub mod parsers;
pub mod conf;
use parsers::ChatParsers;

use std::collections::HashSet;

// For use as a wasm module
use wasm_bindgen::prelude::*;
// We use serde to serialize our structures as JsValue to pass to and from JS code
use serde::{Serialize, Deserialize};
use chrono::prelude::*;


/// Encapsulates a chat message, capturing the sender, date, content, and any media (e.g. images)
/// included in the message.  Media is represented by its src URI, not binary data.
///
/// TODO: have senders represented by something more unique and specific than a string.
/// TODO: store date as not-a-string
#[derive(Debug, Serialize, Deserialize)]
pub struct ChatMessage {
    /* from ChatMessage for conversed */
    pub sender: String,
    pub content: String,
    pub datetime: DateTime<Utc>,

    /* extra; TODO: adapt ChatMessage to support this */
    media: Vec<String>, // list of paths to media, e.g. from IMG.src
}


#[derive(Hash,Debug,Serialize,Deserialize,Eq,PartialEq,Clone)]
pub struct Person {
    name: String,     /* e.g. Adora Grayskull */
    username: String, /* e.g. adorables23 */
    email: String,    /* e.g. ag109@hordemail.net */
}


/// Encapsulates a chat log, including a list of its messages and a set of its participants.
#[derive(Debug, Serialize, Deserialize)]
pub struct ChatLog {
    /* from ChatLog interface for conversed */
    pub name: String,
    pub messages: Vec<ChatMessage>,

    /* extra; TODO: consider adapting ChatLog to support this */
    pub people: HashSet<Person>,
}

impl ChatLog {
    /// Prints the contents to stdout in a human-readable format.
    pub fn print_chat_log (&self) {
        println!("v Chat '{}' with {:?}", self.name, self.people);

        for msg in self.messages.iter ().rev () {
            println!("{}: {}:\n{}", msg.datetime, msg.sender, msg.content);

            for media in &msg.media {
                println!("  IMG[src={}]", media);
            }
        }

        println!("^ Chat with {:?}", self.people);
    }
}

pub trait Parse {
    fn can_parse (&self, file: &String) -> bool;
    fn parse (&self, file: &String) -> Result<Vec<ChatLog>,String>;
    fn parse_data (&self, data : String) -> Result<Vec<ChatLog>,String>;
}

/// Parser for chat logs.  Does not hold specific chatlog state.
/// Does store an identifying string for the service ("fb") and format ("html")
///
/// TODO: have this implement an interface shared by other parsers
pub struct ChatParser {
    parser: Box<dyn Parse+Sync>,
    pub service: String,
    pub format: String,
}

impl Parse for ChatParser {
    fn can_parse (&self, file: &String) -> bool {
        return self.parser.can_parse (file);
    }
    fn parse (&self, file: &String) -> Result<Vec<ChatLog>,String> {
        return self.parser.parse (file);
    }
    fn parse_data (&self, data: String) -> Result<Vec<ChatLog>,String> {
        return self.parser.parse_data (data);
    }
}

fn report_err (msg : String) -> Result<Vec<ChatLog>,String> {
    log (msg.as_str ());
    return Err(msg);
}

/* log:
   - if we're a WASM (without WASI), try use the browser/node's console.log
   - otherwise (WASM with WASI, or Linux, etc.), use println!("");
*/
#[cfg(any (not (target_family = "wasm"), target_family = "wasi"))]
fn log (s : &str) {
    println!("{}", s);
}
#[cfg(all (target_family = "wasm",not (target_os = "wasi")))]
#[wasm_bindgen]
extern "C" {
    // Use `js_namespace` here to bind `console.log(..)` instead of just `log(..)`
    #[wasm_bindgen(js_namespace = console)]
    fn log(s: &str);
}

/// For wasm bindings.  Expose a function parse_chat_log_js () to parse a chatlog as a JS string,
/// parse it into a FbHtmlChatLog, and return it as a JS object
#[wasm_bindgen]
pub fn parse_chat_log_js (service_js: JsValue, data_js : JsValue) -> JsValue {
    utils::set_panic_hook ();

    let service : String = serde_wasm_bindgen::from_value (service_js).unwrap ();
    let data : String = serde_wasm_bindgen::from_value (data_js).unwrap ();

    let parsers : ChatParsers = ChatParsers::new ();
    let parser : ChatParser = match parsers.from_service (&service) {
        Ok(p) => p,
        Err(e) => {
            log(format!("ERROR: {}", e).as_str ());
            return serde_wasm_bindgen::to_value (format!("ERROR: {}", e).as_str ()).unwrap ();
        }
    };

    let chat_logs : Vec<ChatLog> = match parser.parse_data (data) {
        Ok(v) => v,
        Err(e) => return serde_wasm_bindgen::to_value (format!("ERROR: {}", e).as_str ()).unwrap (), //.expect ("Uh oh, failed to parse JsValue as chatlog!");
    };

    return serde_wasm_bindgen::to_value (&chat_logs).unwrap ();
}

PROJ_NAME=conversed
WASI_BIN_DIR=target/wasm32-wasi/debug
TEST_DIR=test_data
META_HTML_2024_TEST_DIR=$(TEST_DIR)/meta_html.2024-02.sp_rf
META_HTML_2020_TEST_DIR=$(TEST_DIR)/meta_html.2020-05-04.shera
CICQ_TEST_DIR=$(TEST_DIR)/cicq.sp_kp

check:
	cargo check

build:
	cargo build

build_wasi: .wasi-generated

build_wasm_pack: .wasm-pack-generated

build_all: build build_wasi build_wasm_pack

cargo_install:
	cargo install --path .

install:
	install -D share/org.kosmokaryote.conversed.desktop ~/.local/share/applications/org.kosmokaryote.conversed.desktop
	install -D share/icons/org.kosmokaryote.conversed.svg ~/.local/share/icons/hicolor/scalable/apps/org.kosmokaryote.conversed.svg
	cargo install --path . --root ~/.local

uninstall:
	rm -f ~/.local/share/applications/org.kosmokaryote.conversed.desktop
	rm -f ~/.local/share/icons/hicolor/scalable/apps/org.kosmokaryote.conversed.svg
	cargo uninstall -p conversed --root ~/.local

doc:
	cargo rustdoc

test: test_rust test_wasm test_wasi

test_rust:
	cargo run -- parse meta_html "${META_HTML_2024_TEST_DIR}/message_1.html"
	cargo run -- parse meta_html "${META_HTML_2020_TEST_DIR}/message_1.formatted.html"
	cargo run -- parse cicq      "${CICQ_TEST_DIR}/"

test_wasi: .wasi-generated
	wasmtime \
		--dir="${WASI_BIN_DIR}" \
		--dir="${META_HTML_2020_TEST_DIR}" \
		"${WASI_BIN_DIR}/${PROJ_NAME}.wasm" \
		"meta_html" "me" \
		"${META_HTML_2020_TEST_DIR}/message_1.compact.html";

test_wasm: .wasm-pack-generated
	(cd examples/nodejs_ts/ && make test)
	(cd examples/conversed-web/ && make test)

clean:
	find . -name "*~" -delete -or -name "#*#" -delete
	rm -rf pkg     # from 'wasm_pack'
	rm -rf target  # from 'cargo build ...'
	rm -rf build-dir        # from flatpak-builder
	rm -rf .flatpak-builder # from flatpak-builder
	rm -f .wasi-generated .wasm-pack-generated
	(cd examples/nodejs_ts/ && make clean)
	(cd examples/conversed-web/ && make clean)

.wasi-generated:
	@echo "WARNING: WASI WASM not currently supported, due to issues with async-std"
	@false
	cargo build --no-default-features --target=wasm32-wasi
	echo "Built in: ${BIN_DIR}/${PROJ_NAME}.wasm";

.wasm-pack-generated: src/*.rs
	wasm-pack build --no-default-features --target=nodejs

flatpak_test: org.kosmokaryote.conversed.yml src/
	flatpak-builder --force-clean --user --install build-dir org.kosmokaryote.conversed.yml

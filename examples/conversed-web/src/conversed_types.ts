/* Borrowed from `conversed` */

import * as mysql from "mysql";

type DbConnectCallback = (dbconn : mysql.Connection) => void;

type ParseCallback = (dbconn : mysql.Connection,
                      chatlog : any,
                      fpath : string,
                      servicename: string) => void;

interface ChatMessage {
  sender: string, /* username, TODO replace with obj or id as names change */
  content: string,
  datetime: Date, /* replace with Date object */
}

interface ChatLog {
  get name (): string, /* user-friend room name */
  get messages (): ChatMessage[],
//  get_participants (): string[], /* list of user names, TODO replace with user objects or IDs as names change */
}

abstract class Parser {
  readonly service: string; /* e.g. "matrix" */
  readonly format: string;  /* e.g. "json"   */

  public abstract parse (conn : mysql.Connection, fpath : string, identify_service_cb : ParseCallback, explore: boolean): void;
}

export { DbConnectCallback, ParseCallback, ChatMessage, ChatLog, Parser };
